#ifndef _STRUC_H_
#define _STRUC_H_

/*Tipovi relokacija:
#define R_ARM_NONE              0
#define R_ARM_PC24              1
#define R_ARM_ABS32             2
#define R_ARM_CALL              28
#define R_ARM_JUMP24            29
#define R_ARM_V4BX              40
#define R_ARM_PREL31            42
#define R_ARM_MOVW_ABS_NC       43
#define R_ARM_MOVT_ABS          44
*/

struct ElfFileHeader{
	unsigned char	e_ident[16];		/* ident bytes */
	unsigned short	e_type;			/* file type */
	unsigned short	e_machine;		/* target machine */
	unsigned int	e_version;		/* file version */
	unsigned int	e_entry;		/* start address */
	unsigned int	e_phoff;		/* phdr file offset */
	unsigned int	e_shoff;		/* shdr file offset */
	unsigned int	e_flags;		/* file flags */
	unsigned short	e_ehsize;		/* sizeof ehdr */
	unsigned short	e_phentsize;		/* sizeof phdr */
	unsigned short	e_phnum;		/* number phdrs */
	unsigned short	e_shentsize;		/* sizeof shdr */
	unsigned short	e_shnum;		/* number shdrs */
	unsigned short	e_shstrndx;		/* shdr string index */

};

struct  ElfHeaderEntry{
	unsigned int sh_name;		/* section name */
	unsigned int sh_type;		/* SHT_... */
	unsigned int sh_flags;		/* SHF_... */
	unsigned int sh_addr;		/* virtual address */
	unsigned int sh_offset;		/* file offset */
	unsigned int sh_size;		/* section size */
	unsigned int sh_link;		/* misc info */
	unsigned int sh_info;		/* misc info */
	unsigned int sh_addralign;	/* memory alignment */
	unsigned int sh_entsize;	/* entry size if table */
};

struct SymTabEntry{
	unsigned int	st_name;
	unsigned int	st_value;
	unsigned int	st_size;
	unsigned char	st_info;	/* bind, type: ELF_32_ST_... */
	unsigned char	st_other;
	unsigned short	st_shndx;	/* SHN_... */
};

struct SectionHeader{
	char *name;
	char *contentPointer;
	unsigned int size;
};

struct ElfRelTextEntry{
	unsigned int	r_offset;
	unsigned int	r_info;		/* sym, type: ELF32_R_... */
};

struct SymbolEntry{
	char *name;
	unsigned int value;
	char bind;
	char type;
	unsigned int sectionLocationIndex;
	char sectionName[20];
};

struct RelTextEntry{
	char *name;
	unsigned int offset;
	unsigned int typeOfRelocation;
};

#endif
