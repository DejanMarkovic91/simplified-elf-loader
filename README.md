Simplified ELF loader
===========

Program was made in Ubuntu operating system version 12.04, and target architecture is ARM x86. Needed tools for 
compiling and testing program are installed as follows:

	– Linaro GNU tools: $sudo apt-get install gcc-arm-linux-gnueabi
	– QEMU Emulator: $ sudo apt-get install qemu-user-static
	– debugger :$ sudo apt-get install gdb-multiarch
	– Pset up variable of IDE: $ export QEMU_LD_PREFIX=/usr/arm-linux-gnueabi
	– g++ compiler: $ sudo apt-get install g++-arm-linux-gnueabi

This program loads and runs staticlly compiled files and runs them. Functions printf and scanf are used from this 
program and called from this main program. Program that is being loaded is specified as command line argument, and 
should be in object file format. Forming of such file is being done with command:

	$ arm-linux-gnueabi-as name_of_file.s –o output_file.o