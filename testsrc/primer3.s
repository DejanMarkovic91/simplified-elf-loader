	.text
	.align	2
	.global	sum
	.type	sum, %function
sum:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	stmfd	sp!, {fp, lr}
	add	fp, sp, #4
	sub	sp, sp, #8
	str	r0, [fp, #-8]
	ldr	r3, [fp, #-8]
	cmp	r3, #1
	bne	.L2
	mov	r3, #1
	b	.L3
.L2:
	ldr	r3, [fp, #-8]
	sub	r3, r3, #1
	mov	r0, r3
	bl	sum
	mov	r2, r0
	ldr	r3, [fp, #-8]
	add	r3, r2, r3
.L3:
	mov	r0, r3
	sub	sp, fp, #4
	ldmfd	sp!, {fp, pc}
	.size	sum, .-sum
	.section	.rodata
	.align	2
.LC0:
	.ascii	"input x: \000"
	.align	2
.LC1:
	.ascii	"%d\000"
	.align	2
.LC2:
	.ascii	"sum %d = %d\012\012\000"
	.text
	.align	2
	.global	main
	.type	main, %function
main:
	stmfd	sp!, {r4, fp, lr}
	add	fp, sp, #8
	sub	sp, sp, #12
	movw	r0, #:lower16:.LC0
	movt	r0, #:upper16:.LC0
	bl	printf
	sub	r3, fp, #16
	movw	r0, #:lower16:.LC1
	movt	r0, #:upper16:.LC1
	mov	r1, r3
	bl	scanf
	ldr	r4, [fp, #-16]
	ldr	r3, [fp, #-16]
	mov	r0, r3
	bl	sum
	mov	r3, r0
	movw	r0, #:lower16:.LC2
	movt	r0, #:upper16:.LC2
	mov	r1, r4
	mov	r2, r3
	bl	printf
	mov	r0, r3
	sub	sp, fp, #8
	ldmfd	sp!, {r4, fp, pc}

