	.section .rodata
msg: 	.asciz "%x"
podatak:.word 0x12345678
	.data
broj: 	.word 4
lr: 	.word 0
pointer:
	.word podatak
.text
.globl main
main:	
	movw r0,#:lower16:broj
	movt r0,#:upper16:broj
	ldr r0,[r0]
	cmp r0,#0
	beq kraj
	b func
kraj:	ldr r0,=poruka
	push {lr}
	bl printf
	ldr r0,=msg
	ldr r1,=pointer
	ldr r1,[r1]
	ldr r1,[r1]
	bl printf
	pop {lr}
	mov pc,lr
	.data 
poruka: .asciz "Poruka\n"
N: .word 4
	.text
func:	
	ldr r0,=broj
	ldr r1,[r0]
	sub r1,r1,#1
	str r1,[r0]

	b main
