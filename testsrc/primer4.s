	.section .rodata
	.equ sim,0x1000
iformat2:
	.asciz "Equ primer 0x1000==0x%x\n"
ptr:	.asciz "String:"	
	.data
	.align 2
iformat:
	.asciz "%s\n"
greska:
	.asciz "Nije uneto nista preko komandne linije!\n"
	.text
	.extern printf
	.global  main
main:
	push	{lr}
	cmp r0,#1
	ble kraj
	add r1,r1,#4
	ldr r1,[r1]
	movw r0,#:lower16:iformat
	movt r0,#:upper16:iformat
	bl printf	
	ldr r0,=iformat2
	mov r1,#sim
	bl printf
	b zavrsi
kraj:	ldr r0,=greska
	bl printf
zavrsi:	pop 	{pc}
	

